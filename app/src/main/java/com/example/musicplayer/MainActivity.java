package com.example.musicplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button startBtn, stopBtn;
    Intent broadIntent;
    public static String TAG = "TAG";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        Intent intent = new Intent(getApplicationContext(), MyService.class);
        startService(intent);
        broadIntent = new Intent();
        startBtn.setOnClickListener(this);
        stopBtn.setOnClickListener(this);
    }

    public void initView() {
        startBtn = findViewById(R.id.btn_start);
        stopBtn = findViewById(R.id.btn_stop);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_start) {
            broadIntent.setAction("start");
//            MyService.myService.musicPlayer.startMusic();
        } else if (v.getId() == R.id.btn_stop) {
            broadIntent.setAction("stop");
//            intent.setAction("stop");
//            MyService.myService.musicPlayer.stopMusic();
        }
        sendBroadcast(broadIntent);
    }
}
