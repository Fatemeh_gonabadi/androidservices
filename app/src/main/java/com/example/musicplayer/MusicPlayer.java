package com.example.musicplayer;

import android.content.Context;
import android.media.MediaPlayer;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.util.Log;

import static com.example.musicplayer.MainActivity.TAG;

public class MusicPlayer extends AsyncTask<Void, Void, Void> {
    Context context;
    MediaPlayer mediaPlayer;

    public MusicPlayer(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    public void stopMusic() {
        Log.d(TAG, "stop");
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
    }

    public void startMusic() {
        Log.d(TAG, "start");
        mediaPlayer = MediaPlayer.create(context, R.raw.patomat);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

}
