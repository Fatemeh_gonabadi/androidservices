package com.example.musicplayer;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;


public class MyService extends Service {
    MusicPlayer musicPlayer;
//    public static MyService myService;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
//        if (myService == null)
//            myService = this;
        musicPlayer = new MusicPlayer(getApplicationContext());
        musicPlayer.execute();
        IntentFilter filter = new IntentFilter();
        filter.addAction("start");
        filter.addAction("stop");
        registerReceiver(receiver, filter);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("start")) {
                musicPlayer.startMusic();

            } else if (action.equals("stop")) {
                musicPlayer.stopMusic();
            }
        }
    };


}
